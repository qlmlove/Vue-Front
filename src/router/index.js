import Vue from 'vue'
import Router from 'vue-router'
// import HelloWorld from '@/components/HelloWorld'
import Archive from '@/components/Archive'
import ArchiveDetail from '@/components/ArchiveDetail'
import ArticleList from '@/components/ArticleList'
import About from '@/components/About'
import Timeline from '@/components/Timeline'
import ArticleDetail from '@/components/ArticleDetail'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'ArticleList',
      component: ArticleList
    }, {
      path: '/archive',
      name: 'Archive',
      component: Archive
    }, {
      path: '/archive/:id',
      name: 'ArchiveDetail',
      component: ArchiveDetail
    }, {
      path: '/about',
      name: 'About',
      component: About
    }, {
      path: '/timeline',
      name: 'Timeline',
      component: Timeline
    }, {
      path: '/article/:id',
      name: 'ArticleDetail',
      component: ArticleDetail
    }
  ]
})
